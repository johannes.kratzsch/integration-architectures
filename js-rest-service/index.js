const express = require('express');
const app = express();

const swaggerUi = require('swagger-ui-express'),
    swaggerDocument = require('./swagger.json');

//https://stackoverflow.com/questions/56328474/origin-http-localhost4200-has-been-blocked-by-cors-policy-in-angular7
const cors = require('cors');

const Salesman = require("./models/SalesmanSchema");
const EvaluationRecord = require("./models/EvaluationRecordSchema");
const User = require("./models/UserSchema");
const Bonus = require("./models/BonusSchema");

const EmployeeRouter = require("./router/EmployeeRouter");
const AuthorizationRouter = require("./router/AuthorizationRouter");
const RecordRouter = require("./router/RecordRouter");
const OrderRouter = require("./router/OrderRouter");
const BonusRouter = require("./router/BonusRouter");

const port = 3000;


require("./db/MongooseClient")();

// Define middleware.
//https://stackoverflow.com/questions/42803394/cors-credentials-mode-is-include
app.use(cors({credentials: true, origin: 'http://localhost:4200'}));
app.use(express.json());
app.use(express.urlencoded({ extended: true} ));

//Api Documentation
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

// Salesman routes
let employeeRouter = new EmployeeRouter(app, Salesman);
employeeRouter.run();

// EvaluationRecord routes
let recordRouter = new RecordRouter(app, EvaluationRecord);
recordRouter.run();

// Bonus routes
let bonusRouter = new BonusRouter(app, Bonus);
bonusRouter.run();

// Authorization routes
let authorizationRouter = new AuthorizationRouter(app, User, 'user');
authorizationRouter.run();

// Sales routes
let orderRouter = new OrderRouter(app, 'orders');
orderRouter.run();

//Default answer
app.get('/*', (req, res) => {
    return res.send('Not a valid request, see <a href="http://localhost:3000/api-docs">documentation</a> for details.');
});

// Start listening
app.listen(port, () => {
    console.log(`Die API des Performance Monitors ist erreichbar unter http://localhost:${port}`);
    console.log(`Die Swagger-Dokumentation http://localhost:${port}/api-docs`);
    console.log('Stellen Sie eine passende VPN-Verbindung sicher!');
});