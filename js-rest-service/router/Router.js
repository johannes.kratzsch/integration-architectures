let Router = class {

	constructor(app, model, path, controller) {

		this.app = app;
		this.path = path;

		this.restController = controller;
	}

	run () {
		this.app.get(`/${this.path}`, this.restController.modelGetAll);

		this.app.get(`/${this.path}/:id`, this.restController.modelGetID);

		this.app.post(`/${this.path}`, this.restController.modelPost);

		this.app.put(`/${this.path}/:id`, this.restController.modelPut);

		this.app.delete(`/${this.path}/:id`, this.restController.modelDelete);
	}
}

module.exports = Router;