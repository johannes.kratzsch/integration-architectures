const OrderController = require('../controllers/OrderController.js');

let OrderRouter = class {

	constructor(app, path, controller) {

		this.app = app;
		this.path = path;

		this.orderController = new OrderController();
	}

	run () {
		this.app.get(`/${this.path}`, this.orderController.modelGetAll);

		this.app.get(`/${this.path}/:salesman_id/:year`, this.orderController.modelGetBySalesmanIdAndYear);
	}
}

module.exports = OrderRouter;