const Router = require("./Router")
const BonusController = require('../controllers/BonusController.js');

let BonusRouter = class extends Router {

    constructor(app, model) {
        super(app, model, 'bonus', new BonusController(model));
        this.restController = new BonusController(model);
    }

    run() {
        this.app.get(`/${this.path}`, this.restController.modelGetAll);

        this.app.post(`/${this.path}`, this.restController.modelPost);

        this.app.get(`/${this.path}/:salesman_id/:year`, this.restController.modelGetBySalesmanIdAndYear);
    }
}

module.exports = BonusRouter;