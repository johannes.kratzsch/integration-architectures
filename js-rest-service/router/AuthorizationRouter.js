const Router = require("./Router")
const cookieParser = require('cookie-parser');
const session = require('express-session');
const UserService = require("../services/UserService");


const AuthorizationController = require('../controllers/AuthorizationController.js');

let AuthorizationRouter =  class extends Router {

	constructor(app, model) {
		super(app, model, 'user', new AuthorizationController(model, new UserService(model)));
		this.userService = new UserService(model);
	}

	run () {
		// initialize cookie-parser
		this.app.use(cookieParser());

		// initialize session management
		this.app.use(session({
			key: 'user_sid',
			secret: '8XtxTSthTtBUT5X2X4YK9Gtd',
			resave: false,
			saveUninitialized: false,
			cookie: {
				expires: 600000
			}
		}));

		// middleware that restores the user from session
		this.app.use((req, res, next) => {
			if (req.session && req.session.userEmail) {
				let user = this.restController.getUserByEmail(req.session.userEmail);

				if (!user) {
					return next(new Error("Couldn't restore user from session."));
				}

				req.user = user;
			}

			return next();
		});

		this.app.post(`/${this.path}/register`, this.restController.modelCreate);

		this.app.post(`/${this.path}/login`, this.restController.modelLogin);

		this.app.post(`/${this.path}/logout`, this.restController.modelLogout);

		this.app.get(`/${this.path}/dashboard`, this.restController.checkLoggedIn, this.restController.modelDashboard);
	}
}

module.exports = AuthorizationRouter;