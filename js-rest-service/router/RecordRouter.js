const Router = require("./Router")
const RecordController = require('../controllers/RecordController.js');

let RecordRouter = class extends Router {

    constructor(app, model) {
        super(app, model, 'records', new RecordController(model));
        this.restController = new RecordController(model);
    }

    run() {
        super.run();

        this.app.get(`/${this.path}/employee/:id`, this.restController.getRecordsByEmployeeID);
        this.app.get(`/${this.path}/employee/:id/year/:year`, this.restController.getRecordByEmployeeIDAndYear);
    }
}

module.exports = RecordRouter;