const Router = require("./Router")
const EmployeeController = require('../controllers/EmployeeController.js');

let EmployeeRouter = class extends Router {

    constructor(app, model) {
        super(app, model, 'salesmen', new EmployeeController(model));

        this.employeeController = new EmployeeController(model);
    }

    run () {
        super.run();

        this.app.get(`/${this.path}/:id/bonus`, this.employeeController.modelGetBonus);

        this.app.post(`/${this.path}/:id/bonus`, this.employeeController.modelPostBonus);
    }
}

module.exports = EmployeeRouter;