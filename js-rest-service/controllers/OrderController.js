const OpenCRXService = require("../services/OpenCRXService");

const ERROR_INTERNAL = 'Es gab einen internen Fehler.';

module.exports = function() {

    let module = {};

    let restService = new OpenCRXService();

    module.modelGetBySalesmanIdAndYear = async function (req, res) {
        try {
            let year = Number(req.params.year);
            const sales = await restService.getSales(req.params.salesman_id);
            const salesOfTheYear = sales.filter(sale => sale.year === year);


            return res.status(201).send(salesOfTheYear);
        } catch (e) {
            return res.status(400).send({  error:  ERROR_INTERNAL, description: String(e) });
        }
    }

    module.modelGetAll = async function (req, res) {
        try {
            const sales = await restService.getAllSales();

            return res.status(201).send(sales);
        } catch (e) {
            return res.status(400).send({  error:  ERROR_INTERNAL, description: String(e) });
        }
    }


    return module;
};
