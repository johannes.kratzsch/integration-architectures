const LoginMessage = require("../messages/LoginMessage");

const UserService = require("../services/UserService");
const mongoose = require('mongoose');

const ERROR_FORMAT = 'Die Anfrage hat ein falsches Format!';
const ERROR_WRONG_CREDENTIALS = 'Falsche Zugangsdaten';
const ERROR_INTERNAL = 'Es gab einen internen Fehler.';

module.exports = function(model, userService) {

    if (userService === undefined)
        throw new Error("Missing Argument: userService");

    if (! (userService instanceof UserService))
        throw new Error("TypeError: Not of Type 'Userservice'");

    let module = {};

    module.modelCreate = async function (req, res) {
        try {
            const newModelDocument = await userService.createNewModelDocument(req.body);

            return res.status(201).send(newModelDocument);
        } catch (e) {
            if (e instanceof mongoose.Error.ValidationError) {
                return res.status(400).send({  error:  ERROR_FORMAT, description: String(e) });
            } else {
                return res.status(500).send({  error:  ERROR_INTERNAL, description: String(e) });
            }
        }
    }

    module.modelLogin = async function (req, res) {
        try {
            let email = req.body.email;
            let password = req.body.password;

            let user = await userService.findModelDocumentByCredentials(email, password);

            if (user === undefined) {
                return res.status(401).send(LoginMessage(false, user));
            }

            //create session
            req.session.userEmail = user.email;
            user.password = "";

            return res.status(200).send(LoginMessage(true, user));
        } catch (e) {
            throw e;

            if (e instanceof mongoose.Error.ValidationError) {
                return res.status(400).send({  error:  ERROR_FORMAT, description: String(e) });
            } else {
                return res.status(500).send({  error:  ERROR_INTERNAL, description: String(e) });
            }
        }
    }

    module.modelLogout = (req, res) => {
        res.clearCookie('user_sid');
        return res.status(200).send("successfully logged out");
    }

    module.modelDashboard = (req, res) => {
        return res.status(200).send("successfully authenticated. you may now access your dashboard");
    }

    module.getUserByEmail = (email) => {
        return userService.findModelDocumentByMail(email);
    }

    // middleware that checks if the user is logged in
    module.checkLoggedIn = (req, res, next) => {
        if (!req.user) {
            return res.status(401).send({  error:  'Not authenticated' });
        }

        return next();
    }


    return module;
};
