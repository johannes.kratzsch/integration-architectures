RestService = require("../services/RestService");

const mongoose = require('mongoose');

const ERROR_FORMAT = 'Die Anfrage hat ein falsches Format!';
const ERROR_INTERNAL = 'Es gab einen internen Fehler.';
const ERROR_INVALID_ID = 'Die ID gibt es nicht!';

module.exports = function(model) {
    let RestController = require("./RestController")(model);
    let restService = new RestService(model);

    RestController.getRecordsByEmployeeID = async function (req, res) {

        try {
            const modelDocuments = await restService.getModelDocumentsByValue({salesman_id: req.params.id});

            if (!modelDocuments) return res.status(404).send({error: ERROR_INVALID_ID});

            return res.send(modelDocuments);
        } catch (e) {
            if (e instanceof mongoose.Error.CastError) {
                return res.status(400).send({error: ERROR_FORMAT, description: String(e)});
            } else {
                return res.status(500).send({error: ERROR_INTERNAL, description: String(e)});
            }
        }
    }

    RestController.getRecordByEmployeeIDAndYear = async function (req, res) {

        try {
            const modelDocuments = await restService.getModelDocumentsByValue({salesman_id: req.params.id, year: req.params.year});

            if (!modelDocuments[0]) return res.status(404).send({error: ERROR_INVALID_ID});

            return res.send(modelDocuments[0]);
        } catch (e) {
            if (e instanceof mongoose.Error.CastError) {
                return res.status(400).send({error: ERROR_FORMAT, description: String(e)});
            } else {
                return res.status(500).send({error: ERROR_INTERNAL, description: String(e)});
            }
        }
    }

    return RestController;
}