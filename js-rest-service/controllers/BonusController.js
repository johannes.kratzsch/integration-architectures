RestService = require("../services/RestService");
OrangeHRMService = require("../services/OrangeHRMService");

const mongoose = require('mongoose');

const ERROR_FORMAT = 'Die Anfrage hat ein falsches Format!';
const ERROR_INTERNAL = 'Es gab einen internen Fehler.';
const ERROR_INVALID_ID = 'Die ID gibt es nicht!';

module.exports = function(model) {

    let module = {};

    let restService = new RestService(model);
    let orangeHRMService = new OrangeHRMService();

    module.modelPost = async function (req, res) {
        try {
            const newModelDocument = await restService.createNewModelDocument(req.body);
            newModelDocument.save();

            await orangeHRMService.addBonus(newModelDocument.salesman_id, newModelDocument.year, newModelDocument.total_bonus);

            return res.status(201).send(newModelDocument);
        } catch (e) {
            if (e instanceof mongoose.Error.ValidationError) {
                return res.status(400).send({  error:  ERROR_FORMAT, description: String(e) });
            } else {
                throw e;
                return res.status(500).send({  error:  ERROR_INTERNAL, description: String(e) });
            }
        }
    }

     module.modelGetAll = async function (req, res) {

        try {
            const modelDocuments = await restService.getModelDocuments();

            if (!modelDocuments) return res.status(404).send({ error: ERROR_INVALID_ID });

            return res.send(modelDocuments);
        } catch (e) {
            if (e instanceof mongoose.Error.CastError) {
                return res.status(400).send({ error: ERROR_FORMAT, description: String(e) });
            } else {
                return res.status(500).send({ error: ERROR_INTERNAL, description: String(e) });
            }
        }
    }

    module.modelGetBySalesmanIdAndYear = async function (req, res) {
        try {
            let modelDocument = await restService.findModelDocumentBySalesmanIdAndYear(req.params.salesman_id, req.params.year);

            if (!modelDocument) return res.status(200).send({});

            return res.send(modelDocument);
        } catch (e) {
            if (e instanceof mongoose.Error.CastError) {
                return res.status(400).send({ error: ERROR_FORMAT, description: String(e) });
            } else {
                return res.status(500).send({ error: ERROR_INTERNAL, description: String(e) });
            }
        }
    }

    return module;

};
