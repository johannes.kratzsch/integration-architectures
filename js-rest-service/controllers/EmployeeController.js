OrangeHRMService = require("../services/OrangeHRMService");
RestService = require("../services/RestService");

const mongoose = require('mongoose');

const ERROR_FORMAT = 'Die Anfrage hat ein falsches Format!';
const ERROR_INTERNAL = 'Es gab einen internen Fehler.';
const ERROR_INVALID_ID = 'Die ID gibt es nicht!';

module.exports = function(model) {

    let orangeHRMService = new OrangeHRMService();

    let module = {};

    module.modelPost = async function (req, res) {
        let employee = orangeHRMService.createEmployee(req.body);

        let newID = employee.id;
        req.body.id = newID;

        return res.status(201).send(req.body);
    }

    module.modelPut = async function (req, res) {
        return res.status(404).send(req.body);
    }

    module.modelDelete = async function (req, res) {
        let id = parseInt(req.params.id);

        return res.send(await orangeHRMService.removeEmployee(id));
    }

    module.modelGetAll = async function (req, res) {
        return res.send(await orangeHRMService.getEmployees());
    }

    module.modelGetID = async function (req, res) {
        let id = req.params.id;

        let modelDocument = await orangeHRMService.getEmployeeByID(id);

        if (!modelDocument) return res.status(404).send({error: ERROR_INVALID_ID});

        return res.send(modelDocument);
    }

    module.modelPostBonus = async function (req, res) {
        return res.send(await orangeHRMService.addBonus(req.params.id, req.body.year, req.body.value));
    }

    module.modelGetBonus = async function (req, res) {
        return res.send(await orangeHRMService.getBonus(req.params.id));
    }

    return module;
}