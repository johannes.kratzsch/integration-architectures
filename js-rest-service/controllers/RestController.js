RestService = require("../services/RestService");

const mongoose = require('mongoose');

const ERROR_FORMAT = 'Die Anfrage hat ein falsches Format!';
const ERROR_INTERNAL = 'Es gab einen internen Fehler.';
const ERROR_INVALID_ID = 'Die ID gibt es nicht!';

module.exports = function(model) {

    let module = {};

    let restService = new RestService(model);

    module.modelPost = async function (req, res) {
        try {
            const newModelDocument = await restService.createNewModelDocument(req.body);
            newModelDocument.save();

            return res.status(201).send(newModelDocument);
        } catch (e) {
            if (e instanceof mongoose.Error.ValidationError) {
                res.status(400).send({  error:  ERROR_FORMAT, description: String(e) });
            } else {
                res.status(500).send({  error:  ERROR_INTERNAL, description: String(e) });
            }

            throw e;
        }
    }

    module.modelPut = async function (req, res) {
        try {
            let id = req.params.id;

            let updatedModelDocument = await restService.updateModelDocument(id, req.body);

            if (!updatedModelDocument) return res.status(404).send({ error: ERROR_INVALID_ID });

            return res.send({ updatedModelDocument });
        } catch (e) {
            if (e instanceof mongoose.Error.CastError) {
                res.status(400).send({ error: ERROR_FORMAT, description: String(e) });
            } else {
                res.status(500).send({ error: ERROR_INTERNAL, description: String(e) });
            }

            throw e;
        }
    }

    module.modelDelete = async function (req, res) {
        try {
            const deletedModelDocument = restService.deleteModelDocument(req.params.id);

            if (!deletedModelDocument) return res.status(404).send({ error: ERROR_INVALID_ID, description: String(e) });

            return res.send({ deletedModelDocument });
        } catch (e) {
            if (e instanceof mongoose.Error.CastError) {
                res.status(400).send({ error: ERROR_FORMAT, description: String(e) });
            } else {
                res.status(500).send({ error: ERROR_INTERNAL, description: String(e) });
            }

            throw e;
        }
    }

     module.modelGetAll = async function (req, res) {

        try {
            const modelDocuments = await restService.getModelDocuments();

            if (!modelDocuments) return res.status(404).send({ error: ERROR_INVALID_ID });

            return res.send(modelDocuments);
        } catch (e) {
            if (e instanceof mongoose.Error.CastError) {
                res.status(400).send({ error: ERROR_FORMAT, description: String(e) });
            } else {
                res.status(500).send({ error: ERROR_INTERNAL, description: String(e) });
            }

            throw e;
        }
    }

    module.modelGetID = async function (req, res) {
        try {
            let id = req.params.id;

            let modelDocument = await restService.findModelDocumentByID(id);

            if (!modelDocument) return res.status(404).send({ error: ERROR_INVALID_ID });

            return res.send(modelDocument);
        } catch (e) {
            if (e instanceof mongoose.Error.CastError) {
                res.status(400).send({ error: ERROR_FORMAT, description: String(e) });
            } else {
                res.status(500).send({ error: ERROR_INTERNAL, description: String(e) });
            }

            throw e;
        }
    }

    return module;

};
