const sinon = require('sinon');
const chai = require('chai');
const expect = chai.expect;

const UserService = require('../services/UserService');
const AuthorizationController = require('../controllers/AuthorizationController.js');
const User = require("../models/UserSchema");

// Sinn dieser Integration Tests: Authentifizierung unabhängig von der MongoDB Anbindung testen
describe('Authentication service', () => {
	it("registration", async () => {
		let documentMock = {test:"test"};

		let userService = new UserService(User);
		sinon.stub(userService, "createNewModelDocument").returns(documentMock);

		let controller = new AuthorizationController(User, userService);

		let requestBody = {
			"firstname": "Peter",
			"lastname": "Müller",
			"type": "salesman",
			"employeeId": 200,
			"email": "abcdefg@gmx.de",
			"password": "Sicher"
		}

		let req = {
			body: requestBody
		}

		let statusStub = sinon.stub().returns({
			send: sinon.spy()
		})

		let res = {
			status: statusStub
		}

		await controller.modelCreate(req, res);

		expect(res.status.firstCall.args[0]).to.equal(201);
		expect(res.status().send.firstCall.args[0]).to.equal(documentMock);
		expect(res.status().send.calledOnce).to.be.true;
	});


	it("login", async () => {
		let userService = new UserService(User);
		let documentMock = {email:"abcdefg@gmx.de"};
		sinon.stub(userService, "findModelDocumentByCredentials").returns(documentMock);

		let controller = new AuthorizationController(User, userService);

		let requestBody = {
			"email": "abcdefg@gmx.de",
			"password": "Sicher"
		}

		let req = {
			body: requestBody,
			session: {}
		}

		let statusStub = sinon.stub().returns({
			send: sinon.spy()
		})

		let res = {
			status: statusStub,
		}

		await controller.modelLogin(req, res);

		expect(res.status.calledOnce).to.be.true;
		expect(res.status.firstCall.args[0]).to.equal(200);
		expect(res.status().send.calledOnce).to.be.true;
		expect(req.session.userEmail).to.equal("abcdefg@gmx.de");
	});



	it("logout", () => {
		let userService = new UserService(User);
		let controller = new AuthorizationController(User, userService);

		let req = {}

		let res = {
			status: function (){},
			clearCookie: sinon.spy()
		}

		let sendSpy = sinon.spy()

		let statusStub = sinon.stub(res, 'status').returns({
			send: sendSpy
		});

		controller.modelLogout(req, res);

		expect(statusStub.calledOnce).to.be.true;
		expect(statusStub.firstCall.args[0]).to.equal(200);
		expect(sendSpy.calledOnce).to.be.true;
		expect(sendSpy.firstCall.args[0]).to.equal("successfully logged out");
		expect(res.clearCookie.calledOnce).to.be.true;
		expect(res.clearCookie.firstCall.args[0]).to.equal('user_sid');
	});
});
