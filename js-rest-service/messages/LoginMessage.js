let LoginMessage = function(successful, userSchema) {
  let loginMessage;

  loginMessage = {successful:successful, account:userSchema};

  return loginMessage;
};

module.exports = LoginMessage;