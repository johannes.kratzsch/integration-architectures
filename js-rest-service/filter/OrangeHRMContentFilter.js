module.exports = function() {

    let module = {};

    module.getFilteredSalesmen = function (axiosResponse) {

        let salesmen = [];

        for (let i = 0; i < axiosResponse.data.length; i++) {
            let filteredObject = {};
            filteredObject._id = axiosResponse.data[i].employeeId;
            filteredObject.firstname = axiosResponse.data[i].firstName;
            filteredObject.lastname = axiosResponse.data[i].lastName;
            filteredObject.department = axiosResponse.data[i].unit;
            salesmen.push(filteredObject);
        }

        return salesmen;
    }

    module.getFilteredSalesman = function (axiosResponse) {

        let filteredObject = {};

        if (axiosResponse) {
            filteredObject._id = axiosResponse.data.employeeId;
            filteredObject.firstname = axiosResponse.data.firstName;
            filteredObject.lastname = axiosResponse.data.lastName;
            filteredObject.department = axiosResponse.data.unit;
        }

        return filteredObject;
    }

    return module;
};