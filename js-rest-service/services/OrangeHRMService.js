qs = require("qs")
axios = require("axios")
FormData = require("form-data");
OrangeHRMContentFilter = require("../filter/OrangeHRMContentFilter")();

const baseUrl = 'https://sepp-hrm.inf.h-brs.de/symfony/web/index.php';
const apiUrl = baseUrl + '/api/v1';

const JOB_SALESMAN = 1;

async function generateToken() {
    const body = qs.stringify({
        client_id: 'api_oauth_id',
        client_secret: 'oauth_secret',
        grant_type: 'password',
        username: 'Lohmüller',
        password: 'Demotlohm12s!'
    });

    const config = {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json'}};

    const res = await axios.post(`${baseUrl}/oauth/issueToken`, body, config);

    if (res.data.error) {
        throw Error(res.data.error);
    }

    return await res.data['access_token'];
}

let OrangeHRMService = class {

    async _createDeleteRequest(formData, apiCall) {
        let token = await generateToken();

        const config = formData.getHeaders();
        config.Authorization = `Bearer ${token}`;

        return axios.delete(
            apiUrl + apiCall,
            { headers: config }
        )
            .then((values) => { return values.data; })
            .catch((error) => { console.log(error); });
    }

    async _createPostRequest(formData, apiCall) {
        let token = await generateToken();

        const config = formData.getHeaders();
        config.Authorization = `Bearer ${token}`;

        return axios.post(
            apiUrl + apiCall,
            formData,
            { headers: config }
        )
            .then((values) => { return values.data; })
            .catch((error) => { console.log(error); });
    }

    async _createPutRequest(formData, apiCall) {
        let token = await generateToken();

        const config = formData.getHeaders();
        config.Authorization = `Bearer ${token}`;

        return axios.put(
            apiUrl + apiCall,
            formData,
            {headers: config}
        )
            .then((values) => {
                return values.data;
            })
            .catch((error) => {
                console.log(error);
            });
    }

    async _createGetRequest(apiCall, params) {
        let token = await generateToken();

        const config = {};
        config.Authorization = `Bearer ${token}`;

        return axios.get(
            apiUrl + apiCall,
            { headers: config, params : params}
        )
            .then((values) => { return values.data; })
            .catch((error) => { console.log(error); });
    }

    //Deprecated
    async _addJobTitle(request, title) {
        let formData = new FormData();
        formData.append('id', request.id);
        formData.append('title', title);

        return this._createPostRequest(formData,`/employee/${request.id}/job-detail`);
    }

    //Deprecated
    async removeEmployee(id) {
        let formData = new FormData();
        formData.append('id', id);
        formData.append('date', '2021-01-01');
        formData.append('reason', 'OrangeHRM');
        formData.append('note', 'Terminated over OrangeHRM');

        return this._createPostRequest(formData,`/employee/${id}/action/terminate`);
    }

    //Deprecated
    async createEmployee(model) {
        let formData = new FormData();
        formData.append('firstName', model.firstname);
        formData.append('middleName', ' ');
        formData.append('lastName', model.lastname);

        let createRequest = await this._createPostRequest(formData,`/employee/99`);
        await this._addJobTitle(createRequest, JOB_SALESMAN);

        return createRequest;
    }

    async addBonus(id, year, bonus) {
        let formData = new FormData();
        formData.append('year', year);
        formData.append('value', bonus);

        return await this._createPostRequest(formData,`/employee/${id}/bonussalary`);
    }

    async getBonus(id) {
        return this._createGetRequest(`/employee/${id}/bonussalary`);
    }

    async getEmployeeByID(id) {
        let axiosResponse = await this._createGetRequest(`/employee/${id}`);
        return OrangeHRMContentFilter.getFilteredSalesman(axiosResponse);
    }

    //Deprecated
    async updateModelByID(id, body) {
        let formData = new FormData();

        if (this._isSalesman(body)) {
            if (body.firstname !== undefined) {
                formData.append('firstName', body.firstname);
            }
            if (body.lastname !== undefined) {
                formData.append('lastName', body.lastname);
            }
        }
        return await this._createPutRequest(formData,`/employee/${id}`);
    }

    _isSalesman(object) {
        return object.firstname !== undefined;
    }

    async getEmployees() {
        let axiosResponse = await this._createGetRequest('/employee/search', {'jobTitle' : 1});
        return OrangeHRMContentFilter.getFilteredSalesmen(axiosResponse);
    }

};

module.exports = OrangeHRMService;