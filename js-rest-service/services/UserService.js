const bcrypt = require('bcrypt');

let UserService = class {

    constructor(model) {
        this._model = model;
    }

    createNewModelDocument = async function (postBody) {
        let modelDocument = new this._model(postBody);
        await modelDocument.save();

        return modelDocument;
    }


    findModelDocumentByCredentials = async function (email, password) {
        let user = await this._model.findOne({ email: email });

        if (!user)
            return undefined;

        const validPassword = await bcrypt.compare(password, user.password);

        if (!validPassword)
            return undefined;

        return user;
    }

    findModelDocumentByMail = async function (email) {
        let user = await this._model.findOne({ email: email });

        return user;
    }




};

module.exports = UserService;