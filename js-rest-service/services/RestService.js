qs = require("qs")
axios = require("axios")

let RestService = class {

    constructor(model) {
        this._model = model;
    }

    getModelDocuments = function () {
        return this._model.find();
    }

    getModelDocumentsByValue = function (options) {
        return this._model.find(options);
    }

    createNewModelDocument = async function (postBody) {

        if ('_id' in postBody) {
            delete postBody._id;
        }

        let modelDocument = new this._model(postBody);
        await modelDocument.save();

        return modelDocument;
    }

    findModelDocumentByID = async function (id) {
        return this._model.findById(id);
    }

    findModelDocumentBySalesmanIdAndYear = async function (salesman_id, year) {
        return this._model.findOne({salesman_id: salesman_id, year: year});
    }

    updateModelDocument = async function (id, updates) {
        if ('_id' in updates) {
            delete updates._id;
        }

        let updateModelDocument = await this._model.findByIdAndUpdate(id, updates, { runValidators: true, new: true });
        return updateModelDocument;
    }

    deleteModelDocument = async function (id) {
        return this._model.findByIdAndDelete(id);
    }

};

module.exports = RestService;