qs = require("qs")
axios = require("axios")

const baseUrl = 'https://sepp-crm.inf.h-brs.de/opencrx-rest-CRX';
const salesAPI = `${baseUrl}/org.opencrx.kernel.contract1/provider/CRX/segment/Standard/salesOrder`

const credentials = {
    username: 'guest',
    password: 'guest',
};

const requestConfig = {
    headers: {
        'Accept': 'application/json'
    },
    auth: credentials,
};

const salesmanIdMapping = {
    9: 'L0NTAXG7TQTPM0EBHQA5MAZ7J',
    2: '9ENFSDRCBESBTH2MA4T2TYJFL'
}

let OpenCRXService = class {
    filterSales(sales, salesman_id) {
        return sales.filter(
            sale => sale.salesRep['@href'].includes(salesmanIdMapping[salesman_id])
        );
    }

    async getCompanyName(sale) {
        const url = sale.customer['@href'];
        let response = await axios.get(url, requestConfig);
        return response.data.name;
    }

    async getSales(salesman_id = null) {
        let sales = [];

        let response = await axios.get(salesAPI, requestConfig);
        let rawSales = response.data.objects;

        if (salesman_id !== null) {
            rawSales  = this.filterSales(rawSales, salesman_id);
        }

        for (const sale of rawSales) {
            let company = await this.getCompanyName(sale);
            const ranking = company.charCodeAt(0) % 3 + 3; //Random, deterministic client ranking

            sales.push({
                product: 'HooverGo',
                quantity: Number(sale.totalAmount),
                client: company,
                client_ranking: ranking,
                year: 2021
            });
        }

        return sales;
    }

    async getAllSales() {
        return this.getSales(1111);
    }

};

module.exports = OpenCRXService;