const mongoose = require("mongoose");
const bcrypt = require('bcrypt');
const SALT_WORK_FACTOR = 10;

const userSchema = new mongoose.Schema({
	firstname: {
		type: String,
		trim: true,
		required: true
	},
	lastname: {
		type: String,
		trim: true,
		required: true
	},
	type: {
		type: String,
		required: true,
		enum: ['salesman', 'hr', 'ceo'] //creates a validator that checks if the value is in the given array.
	},
	employeeId: {
		type: Number,
		required: true
	},
	email: {
		type: String,
		required: true,
		index: { unique: true }
	},
	password: {
		type: String,
		required: true
	},
}, {versionKey: false});

//Source for password hashing: https://www.mongodb.com/blog/post/password-authentication-with-mongoose-part-1
userSchema.pre('save', function(next) {
	var user = this;

	// only hash the password if it has been modified (or is new)
	if (!user.isModified('password')) return next();

	// generate a salt
	bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
		if (err) return next(err);

		// hash the password using our new salt
		bcrypt.hash(user.password, salt, function(err, hash) {
			if (err) return next(err);
			// override the cleartext password with the hashed one
			user.password = hash;
			next();
		});
	});
});

userSchema.methods.comparePassword = function(candidatePassword, cb) {
	return bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
		if (err) return cb(err, false);

		return cb(null, isMatch);
	});
};

const UserSchema = mongoose.model('User', userSchema);

module.exports = UserSchema;