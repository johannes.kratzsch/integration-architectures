const mongoose = require("mongoose")

const evaluationRecordSchema = new mongoose.Schema({
	attitude: {
		type: Number,
		required: true
	},
	integrity: {
		type: Number,
		required: true
	},
	leadership: {
		type: Number,
		required: true
	},
	communications: {
		type: Number,
		required: true
	},
	openness: {
		type: Number,
		required: true
	},
	social: {
		type: Number,
		required: true
	},
	attitude_bonus: {
		type: Number,
		required: true
	},
	integrity_bonus: {
		type: Number,
		required: true
	},
	leadership_bonus: {
		type: Number,
		required: true
	},
	communications_bonus: {
		type: Number,
		required: true
	},
	openness_bonus: {
		type: Number,
		required: true
	},
	social_bonus: {
		type: Number,
		required: true
	},
	salesman_id: {
		type: Number,
		required: true
	},
	year: {
		type: Number,
		required: true,
		default: 2021
	}
}, {versionKey: false});

const EvaluationRecordSchema = mongoose.model('EvaluationRecord', evaluationRecordSchema);

module.exports = EvaluationRecordSchema;