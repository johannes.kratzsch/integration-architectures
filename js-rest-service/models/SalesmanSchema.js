const mongoose = require("mongoose")


const salesmanSchema = new mongoose.Schema({
	firstname: {
		type: String,
		trim: true,
		required: true
	},
	lastname: {
		type: String,
		trim: true,
		required: true
	},
}, {versionKey: false});

const SalesmanSchema = mongoose.model('Salesman', salesmanSchema);

module.exports = SalesmanSchema;