const mongoose = require("mongoose")

const bonusSchema = new mongoose.Schema({
	salesman_id: {
		type: Number,
		required: true
	},
	year: {
		type: Number,
		default: 2021
	},
	order_bonus: {
		type: Number
	},
	social_performance_bonus: {
		type: Number
	},
	total_bonus: {
		type: Number
	},
	comment: {
		type: String
	},
	approved: {
		type: Boolean,
		default: false
	},
}, {versionKey: false});

const BonusSchema = mongoose.model('Bonus', bonusSchema);

module.exports = BonusSchema;