const mongoose = require('mongoose');

const DATABASE_NAME = 'performanceManagement';
const MONGODB_URL = 'mongodb://localhost:27017/';

module.exports = () => {
	return mongoose.connect(MONGODB_URL + DATABASE_NAME, {
		useNewUrlParser: true,
		useCreateIndex: true,
		useFindAndModify: false
	});
};
