package de.hbrs.performanceMonitor;

import de.hbrs.performanceMonitor.MongoDBClient;
import de.hbrs.performanceMonitor.model.EvaluationRecord;
import de.hbrs.performanceMonitor.model.Salesman;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MongoDBClientTest {

    private MongoDBClient client = new MongoDBClient();
    private Salesman steve;

    @BeforeEach
    void setUp() {
        steve = new Salesman("Steve", "Wozniak");
    }

    @Test
    @DisplayName("Create and Read (CR)")
    void insertAndRetrieveSalesMan() {
        String id = client.createSalesman(steve);

        Salesman retrieved = client.getSalesman(id);

        assertNotNull(retrieved);
        assertEquals("Steve", retrieved.get("firstname"));
        assertEquals("Wozniak", retrieved.get("lastname"));

        client.deleteSalesman(id);
    }

    @Test
    @DisplayName("Update (U)")
    void updateSalesMan() {
        Salesman peter = new Salesman("Peter", "Lustig");

        String id = client.createSalesman(steve);
        client.updateSalesman(id, peter);

        Salesman retrieved = client.getSalesman(id);

        assertNotNull(retrieved);
        assertEquals("Peter", retrieved.get("firstname"));
        assertEquals("Lustig", retrieved.get("lastname"));

        client.deleteSalesman(id);
    }

    @Test
    @DisplayName("Delete (D)")
    void deleteSalesMan() {
        String id = client.createSalesman(steve);
        client.deleteSalesman(id);

        assertNull(client.getSalesman(id));
    }


    @Test
    @DisplayName("Test the EvaluationRecord collection from the database according to the CRUD pattern")
    public void testEvaluationRecordCRUD() {

        //Create Employee for testing
        String salesmanId = client.createSalesman(new Salesman("Dieter", "Wolf9997654"));
        List<Salesman> salesmen = client.querySalesman("lastname", "Wolf9997654");

        //Create
        long beforeSize = client.getEvaluationRecordCount();

        String recordId = client.createEvaluationRecord(new EvaluationRecord(1,2,3,4,5,6, salesmanId));

        assertEquals(beforeSize + 1, client.getEvaluationRecordCount());

        //Read
        EvaluationRecord record = client.getEvaluationRecord(recordId);

        assertEquals(4, record.getAttitude());

        //Update
        client.updateEvaluationRecord(recordId, new EvaluationRecord(7,8,9,10,11,12, salesmanId));

        //Read
        record = client.getEvaluationRecord(recordId);

        assertEquals(11, record.getCommunications());

        //Delete
        client.deleteSalesman(salesmanId);
        client.deleteEvaluationRecord(recordId);

        long afterSize = client.getEvaluationRecordCount();
        assertEquals(beforeSize, afterSize);

        //Delete Employee for testing purpose
        client.deleteSalesman(recordId);
    }
}