package de.hbrs.performanceMonitor.service;

import de.hbrs.performanceMonitor.MongoDBClient;
import de.hbrs.performanceMonitor.model.EvaluationRecord;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EvaluationRecordServiceImpl implements EvaluationRecordService {

    MongoDBClient client = new MongoDBClient();

    @Override
    public boolean create(EvaluationRecord salesMan) {
        return client.createEvaluationRecord(salesMan) != null;
    }

    @Override
    public EvaluationRecord get(String id) {
        return client.getEvaluationRecord(id);
    }

    @Override
    public List<EvaluationRecord> getAll() {
        return client.queryEvaluationRecord(null, null);
    }

    @Override
    public boolean update(String id, EvaluationRecord salesMan) {
        return client.updateEvaluationRecord(id, salesMan);
    }

    @Override
    public boolean delete(String id) {
        return client.deleteEvaluationRecord(id);
    }
}
