package de.hbrs.performanceMonitor.service;

import de.hbrs.performanceMonitor.model.Salesman;

public interface SalesmanService extends CRUDService<Salesman> {

}
