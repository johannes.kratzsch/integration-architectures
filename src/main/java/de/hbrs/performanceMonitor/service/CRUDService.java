package de.hbrs.performanceMonitor.service;

import java.util.List;

public interface CRUDService<T> {
    boolean create(T t);

    T get(String id);

    List<T> getAll();

    boolean update(String id, T t);

    boolean delete(String id);
}
