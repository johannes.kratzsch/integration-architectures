package de.hbrs.performanceMonitor.service;

import de.hbrs.performanceMonitor.MongoDBClient;
import de.hbrs.performanceMonitor.model.Salesman;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SalesmanServiceImpl implements SalesmanService {

    MongoDBClient client = new MongoDBClient();

    @Override
    public boolean create(Salesman salesMan) {
        return client.createSalesman(salesMan) != null;
    }

    @Override
    public Salesman get(String id) {
        return client.getSalesman(id);
    }

    @Override
    public List<Salesman> getAll() {
        return client.querySalesman(null, null);
    }

    @Override
    public boolean update(String id, Salesman salesMan) {
        return client.updateSalesman(id, salesMan);
    }

    @Override
    public boolean delete(String id) {
        return client.deleteSalesman(id);
    }
}
