package de.hbrs.performanceMonitor.service;

import de.hbrs.performanceMonitor.model.EvaluationRecord;

public interface EvaluationRecordService extends CRUDService<EvaluationRecord> {

}
