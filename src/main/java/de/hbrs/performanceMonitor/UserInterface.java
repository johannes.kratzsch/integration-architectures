package de.hbrs.performanceMonitor;

import de.hbrs.performanceMonitor.model.Salesman;

import java.util.List;
import java.util.Scanner;

public class UserInterface {

    public static void main(String[] args) {
        int x1, x2;
        String firstname, lastname;
        Salesman salesMan;
        Scanner sc = new Scanner(System.in);
        MongoDBClient client = new MongoDBClient();

        while (true) {
            System.out.println("Bitte wählen Sie eine Option aus: ");
            System.out.println("1. Verkäufer anlegen");
            System.out.println("2. Verkäuferliste anzeigen");
            x1 = sc.nextInt();

            if (x1 == 1) {
                System.out.println("Vorname?");
                firstname = sc.next();

                System.out.println("Nachname?");
                lastname = sc.next();

                salesMan = new Salesman(firstname, lastname);
                client.createSalesman(salesMan);
                System.out.println("Verkäufer wurde erfolgreich angelegt.");
            } else if (x1 == 2) {
                List<Salesman> salesmen = client.querySalesman(null, null);
                System.out.print(salesmen);
            }
        }
    }
}