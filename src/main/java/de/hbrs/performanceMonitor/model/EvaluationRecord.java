package de.hbrs.performanceMonitor.model;

import org.bson.Document;

public class EvaluationRecord extends Document {
    public static final String MONGODB_ID = "_id";
    public static final String RECORD_ID = "id";
    public static final String RECORD_OPENNESS = "openness";
    public static final String RECORD_LEADERSHIP_COMPETENCE = "leadership";
    public static final String RECORD_SOCIAL = "social";
    public static final String RECORD_ATTITUDE = "attitude";
    public static final String RECORD_COMMUNICATIONS = "communications";
    public static final String RECORD_INTEGRITY = "integrity";
    public static final String RECORD_SALESMAN_ID = "salesman_id";

    public EvaluationRecord(int leadershipCompetence, int openness, int social, int attitude, int communications, int integrity, String salesmanId) {
        setLeadershipCompetence(leadershipCompetence);
        setOpenness(openness);
        setSocial(social);
        setAttitude(attitude);
        setCommunications(communications);
        setIntegrity(integrity);
        setSalesmanId(salesmanId);
    }

    public static EvaluationRecord createFromDocument(Document document) {
        if (document == null) {
            return null;
        }

        EvaluationRecord record = new EvaluationRecord(
                document.getInteger(RECORD_LEADERSHIP_COMPETENCE),
                document.getInteger(RECORD_OPENNESS),
                document.getInteger(RECORD_SOCIAL),
                document.getInteger(RECORD_ATTITUDE),
                document.getInteger(RECORD_COMMUNICATIONS),
                document.getInteger(RECORD_INTEGRITY),
                document.getString(RECORD_SALESMAN_ID)
        );
        record.setId(document.get(MONGODB_ID).toString());
        return record;
    }

    // Get
    public int getLeadershipCompetence() {
        return getInteger(RECORD_LEADERSHIP_COMPETENCE);
    }

    public int getOpenness() {
        return getInteger(RECORD_OPENNESS);
    }

    public int getSocial() {
        return getInteger(RECORD_SOCIAL);
    }

    public int getAttitude() {
        return getInteger(RECORD_ATTITUDE);
    }

    public int getCommunications() {
        return getInteger(RECORD_COMMUNICATIONS);
    }

    public int getIntegrity() {
        return getInteger(RECORD_INTEGRITY);
    }

    public String getSalesmanId() {
        return getString(RECORD_SALESMAN_ID);
    }

    public String getId() {
        return getString(RECORD_ID);
    }

    // Set
    public void setLeadershipCompetence(int leadershipCompetence) {
        append(RECORD_LEADERSHIP_COMPETENCE, leadershipCompetence);
    }

    public void setOpenness(int openness) {
        append(RECORD_OPENNESS, openness);
    }

    public void setSocial(int social) {
        append(RECORD_SOCIAL, social);
    }

    public void setAttitude(int attitude) {
        append(RECORD_ATTITUDE, attitude);
    }

    public void setCommunications(int communications) {
        append(RECORD_COMMUNICATIONS, communications);
    }

    public void setIntegrity(int integrity) {
        append(RECORD_INTEGRITY, integrity);
    }

    public void setSalesmanId(String salesmanId) {
        append(RECORD_SALESMAN_ID, salesmanId);
    }

    public void setId(String id) {
        append(RECORD_ID, id);
    }

}
