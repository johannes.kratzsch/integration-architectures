package de.hbrs.performanceMonitor.model;

import org.bson.Document;

public class Salesman extends Document {

    public static final String MONGODB_ID = "_id";
    public static final String SALESMAN_ID = "id";
    public static final String SALESMAN_FIRSTNAME = "firstname";
    public static final String SALESMAN_LASTNAME = "lastname";

    public Salesman(String firstname, String lastname) {
        setFirstname(firstname);
        setLastname(lastname);
    }

    public static Salesman createFromDocument(Document document) {
        if (document == null) {
            return null;
        }

        Salesman salesman = new Salesman(
                document.getString(SALESMAN_FIRSTNAME),
                document.getString(SALESMAN_LASTNAME)
        );
        salesman.setId(document.get(MONGODB_ID).toString());
        return salesman;
    }

    public void setFirstname(String firstname) {
        append(SALESMAN_FIRSTNAME, firstname);
    }

    public void setLastname(String firstname) {
        append(SALESMAN_LASTNAME, firstname);
    }

    private void setId(String id) {
        append(SALESMAN_ID, id);
    }

    public String getFirstname() {
        return getString(SALESMAN_FIRSTNAME);
    }

    public String getLastname() {
        return getString(SALESMAN_LASTNAME);
    }

    public String getId() {
        return getString(SALESMAN_ID);
    }
}
