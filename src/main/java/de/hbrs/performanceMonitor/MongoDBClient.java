package de.hbrs.performanceMonitor;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import de.hbrs.performanceMonitor.model.EvaluationRecord;
import de.hbrs.performanceMonitor.model.Salesman;
import org.bson.BsonValue;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.LinkedList;
import java.util.List;

import static com.mongodb.client.model.Filters.*;

public class MongoDBClient implements ManagePersonal {
    private static final String HOST_ADDRESS = "mongodb://localhost:27017";
    private static final String DATABASE = "performanceManagement";

    private static final String SALESMEN_COLLECTION = "salesmen";
    private static final String RECORD_COLLECTION = "records";


    //Always start mongodb before using 'mongod' in the terminal
    private final MongoClient client = MongoClients.create(HOST_ADDRESS);
    private final MongoDatabase db = client.getDatabase(DATABASE);
    private final MongoCollection<Document> salesmen = db.getCollection(SALESMEN_COLLECTION);
    private final MongoCollection<Document> records = db.getCollection(RECORD_COLLECTION);

    private static String bsonValueIdToString(BsonValue bsonValue) {
        return bsonValue.asObjectId().getValue().toString();
    }

    private static ObjectId stringToObjectId(String string) {
        return new ObjectId(string);
    }


    // Salesmen:
    public String createSalesman(Salesman record) {
        return bsonValueIdToString(salesmen.insertOne(record).getInsertedId());
    }

    public Salesman getSalesman(String id) {
        return Salesman.createFromDocument(
                salesmen.find(eq("_id", stringToObjectId(id))).first()
        );
    }

    public List<Salesman> querySalesman(String attribute, String key) {
        List<Salesman> salesmenList = new LinkedList<>();

        if (attribute == null) {
            this.salesmen
                .find()
                .forEach(e -> salesmenList.add(Salesman.createFromDocument(e)));

        } else {
            this.salesmen
                .find(eq(attribute, key))
                .forEach(e -> salesmenList.add(Salesman.createFromDocument(e)));
        }

        return salesmenList;
    }

    public boolean updateSalesman(String id, Salesman newRecord) {
        return salesmen.updateOne(
                eq("_id", stringToObjectId(id)),
                new Document("$set", newRecord)
        ).wasAcknowledged();
    }

    public boolean deleteSalesman(String id) {
        return salesmen.deleteOne(eq("_id", stringToObjectId(id)))
                .wasAcknowledged();
    }

    public long getSalesmanCount() {
        return salesmen.countDocuments();
    }


    // Evaluation Records:
    public String createEvaluationRecord(EvaluationRecord record) {
        return bsonValueIdToString(records.insertOne(record).getInsertedId());
    }

    public EvaluationRecord getEvaluationRecord(String id) {
        return EvaluationRecord.createFromDocument(
                records.find(eq("_id", stringToObjectId(id))).first()
        );
    }

    public List<EvaluationRecord> queryEvaluationRecord(String attribute, String key) {
        List<EvaluationRecord> recordList = new LinkedList<>();

        if (attribute == null) {
            this.records
                    .find()
                    .forEach(e -> recordList.add(EvaluationRecord.createFromDocument(e)));

        } else {
            this.records
                    .find(eq(attribute, key))
                    .forEach(e -> recordList.add(EvaluationRecord.createFromDocument(e)));
        }

        return recordList;
    }

    public boolean updateEvaluationRecord(String id, EvaluationRecord newRecord) {
        return records.updateOne(
                eq("_id", stringToObjectId(id)),
                new Document("$set", newRecord)
        ).wasAcknowledged();
    }

    public boolean deleteEvaluationRecord(String id) {
        return records.deleteOne(eq("_id", stringToObjectId(id)))
                .wasAcknowledged();
    }

    public long getEvaluationRecordCount() {
        return records.countDocuments();
    }
}
