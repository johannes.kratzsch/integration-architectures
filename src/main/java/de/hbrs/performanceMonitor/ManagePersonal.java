package de.hbrs.performanceMonitor;

import de.hbrs.performanceMonitor.model.EvaluationRecord;
import de.hbrs.performanceMonitor.model.Salesman;
import org.bson.Document;

import java.util.LinkedList;
import java.util.List;

import static com.mongodb.client.model.Filters.eq;

public interface ManagePersonal {

    // Salesmen:
    public String createSalesman(Salesman record );

    public Salesman getSalesman(String sid );

    public List<Salesman> querySalesman(String attribute , String key );

    public boolean updateSalesman(String sid, Salesman newRecord);

    public boolean deleteSalesman(String sid);


    // Evaluation Records:
    public String createEvaluationRecord(EvaluationRecord record);

    public EvaluationRecord getEvaluationRecord(String id);

    public List<EvaluationRecord> queryEvaluationRecord(String attribute, String key);

    public boolean updateEvaluationRecord(String id, EvaluationRecord newRecord);

    public boolean deleteEvaluationRecord(String id);
}
