package de.hbrs.performanceMonitor.controller;

import de.hbrs.performanceMonitor.model.EvaluationRecord;
import de.hbrs.performanceMonitor.service.EvaluationRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/records") //Erreichbar unter http://localhost:8080/records
public class EvaluationRecordController {
    @Autowired
    EvaluationRecordService evaluationRecordService;

    @GetMapping
    public List<EvaluationRecord> getAll() {
        return evaluationRecordService.getAll();
    }

    @GetMapping("/{id}")
    public EvaluationRecord get(@PathVariable (required = true) String id) {
        return evaluationRecordService.get(id);
    }

    @ResponseStatus(value = HttpStatus.OK, reason = "Operation successfull")
    @PostMapping
    public void create(@RequestBody (required = true) EvaluationRecord record) {
        evaluationRecordService.create(record);

    }

    @ResponseStatus(value = HttpStatus.OK, reason = "Operation successfull")
    @PutMapping("/{id}")
    public void update(@PathVariable(required = true) String id, @RequestBody (required = true) EvaluationRecord record) {
        evaluationRecordService.update(id, record);
    }

    @ResponseStatus(value = HttpStatus.OK, reason = "Operation successfull")
    @DeleteMapping("{id}")
    public void delete(@PathVariable(required = true) String id) {
        evaluationRecordService.delete(id);
    }

}
