package de.hbrs.performanceMonitor.controller;

import de.hbrs.performanceMonitor.model.Salesman;
import de.hbrs.performanceMonitor.service.SalesmanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/salesmen") //Erreichbar unter http://localhost:8080/salesmen
public class SalesmanController {
    @Autowired
    SalesmanService salesmanService;

    @GetMapping
    public List<Salesman> getAll() {
        return salesmanService.getAll();
    }

    @GetMapping("/{id}")
    public Salesman get(@PathVariable (required = true) String id) {
        return salesmanService.get(id);
    }

    @ResponseStatus(value = HttpStatus.OK, reason = "Operation successfull")
    @PostMapping
    public void create(@RequestBody (required = true) Salesman salesman) {
        salesmanService.create(salesman);

    }

    @ResponseStatus(value = HttpStatus.OK, reason = "Operation successfull")
    @PutMapping("/{id}")
    public void update(@PathVariable(required = true) String id, @RequestBody (required = true) Salesman salesman) {
        salesmanService.update(id, salesman);
    }

    @ResponseStatus(value = HttpStatus.OK, reason = "Operation successfull")
    @DeleteMapping("{id}")
    public void delete(@PathVariable(required = true) String id) {
        salesmanService.delete(id);
    }

}
