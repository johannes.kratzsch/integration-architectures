import { Component, OnInit } from '@angular/core';
import {AuthorizationService} from "./services/authorization.service";
import {Router} from "@angular/router";
import {PageService} from "./services/page.service";
import {first} from "rxjs/operators";
import { HttpClient } from "@angular/common/http";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})

export class AppComponent implements OnInit {
  title = 'Performance Cockpit';

  constructor(private http: HttpClient, public authorizationService: AuthorizationService, public pageService: PageService, private route: Router) {

  }

  async ngOnInit(): Promise<void> {
    this.authorizationService.loggedIn = await this.authorizationService.isLoggedIn();

    if (this.authorizationService.loggedIn) {

      await this.route.navigate(['/dashboard']);
    }

    this.http.get('http://localhost:3000', { observe: 'response' })
      .pipe(first())
      .subscribe(resp => {

      }, err => {
        if (err.status == 0)
          this.pageService.changeNotification("Can not connect to the server at localhost:3000")
      });
  }

}
