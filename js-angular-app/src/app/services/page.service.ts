import { Injectable } from '@angular/core';
import {Employee} from "../models/employee";
import {Record} from "../models/record";
import {RestService} from "./rest.service";
import {Bonus} from "../models/bonus";
import {Order} from "../models/order";

@Injectable({
  providedIn: 'root'
})
export class PageService {

  readonly default_record:Record = { _id:"", attitude:0, salesman_id:0, year: 2021, social: 0, openness: 0, communications: 0, leadership: 0, integrity: 0,
    attitude_bonus:0, social_bonus: 0, openness_bonus: 0, communications_bonus: 0, leadership_bonus: 0, integrity_bonus: 0};

  notificationContent = '';

  employeeList: Employee[] = [];
  recordList: Record[] = [];

  currentRecord:Record = { _id:"", attitude:0, salesman_id:0, year: 2021, social: 0, openness: 0, communications: 0, leadership: 0, integrity: 0,
                                    attitude_bonus:0, social_bonus: 0, openness_bonus: 0, communications_bonus: 0, leadership_bonus: 0, integrity_bonus: 0};

  currentEmployee:Employee = {_id:0, firstname:"", lastname:"", department:""}


  bonus: Bonus = this.getEmptyBonus();

  orders : Order[] = []

  constructor() {}

  isCurrentEmployeeEmpty() {
    return this.currentEmployee._id == 0;
  }

  isCurrentRecordEmpty() {
    return this.currentRecord._id == "";
  }

  setCurrentRecordToDefault() {
    let year = this.currentRecord.year;
    let id = this.currentRecord.salesman_id;

    this.currentRecord = this.default_record;
    this.currentRecord.year = year;
    this.currentRecord.salesman_id = id;
  }

  changeNotification(notification : string) {
    this.notificationContent = notification;
  }

  setEmployeeList(userList: Employee[]) {
    this.employeeList = userList;
  }

  setRecordList(recordList: Record[]) {
    this.recordList = recordList;
  }

  updateRecordDialog(record: Record, salesman_id = 0) {
    this.currentRecord = record;
    this.currentRecord.salesman_id = salesman_id;
  }

  updateEmployeeDialog(user: Employee) {
    this.currentEmployee = user;
  }

  // ======== BONUS =========
  resetBonus() {
    this.bonus = this.getEmptyBonus();
  }

  private getEmptyBonus() {
    return {
      salesman_id : this.currentRecord.salesman_id,
      year: this.currentRecord.year,
      order_bonus : 0,
      social_performance_bonus : 0,
      total_bonus : 0,
      comment : "",
      approved : false
    }
  }

  private recalculateTotalBonus() {
    this.bonus.total_bonus = this.bonus.order_bonus + this.bonus.social_performance_bonus;
  }

  setOrderBonus() {
    this.bonus.order_bonus = this.orders
      .map(order => order.bonus)
      .reduce((a, b) => a + b, 0)

    this.recalculateTotalBonus();
  }

  setSocialPerformanceBonus() {
    this.bonus.social_performance_bonus =
      this.currentRecord.attitude_bonus +
      this.currentRecord.social_bonus +
      this.currentRecord.openness_bonus +
      this.currentRecord.communications_bonus +
      this.currentRecord.leadership_bonus +
      this.currentRecord.integrity_bonus;

    this.recalculateTotalBonus();
  }
}
