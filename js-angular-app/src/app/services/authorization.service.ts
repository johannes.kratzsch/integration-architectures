import {Injectable, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from "@angular/router";
import {PageService} from "./page.service";
import {LoginMessage} from "../messages/LoginMessage";
import {Account} from "../models/account";
import {accountType} from "../models/accountType";

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService implements OnInit {

  baseUrl = "http://localhost:3000";
  loggedIn = false;
  activeAccount : Account = {email:"", password:"", firstname : "", lastname : "", type : accountType.salesman, employeeId : 0};

  constructor(private http : HttpClient, private router: Router, private pageService: PageService) {}

  async ngOnInit(): Promise<void> {
    this.loggedIn = await this.isLoggedIn();
  }

  async login(email: string, password: string) : Promise<LoginMessage> {
    let body = {email: email, password: password};

    return this.http.post(this.baseUrl + "/user/login", body, {withCredentials: true}).toPromise().then(res => {
      return <LoginMessage> res;
    }, err =>
    {
      return <LoginMessage> err;
    });
  }

  setActiveAccount(account: Account) {
    localStorage.setItem("activeAccount", JSON.stringify(account));
    this.activeAccount = account;
  }

  register(user:Account) {
    let postRequest = this.http.post(this.baseUrl + "/user/register", user, {responseType: "text"}).subscribe(res => {
      this.router.navigate(["/login"]);
      this.loggedIn = false;
    });

    return postRequest;
  }

  async isLoggedIn() : Promise<boolean> {
    return this.http.get(this.baseUrl + "/user/dashboard", {responseType: "text", withCredentials: true}).toPromise().then(res => {
      return true;
    }, err => {
      return false;
    });
  }

  logout() {
   this.http.post(this.baseUrl + "/user/logout", {}, {responseType: "text", withCredentials: true}).subscribe(
      res => {
        this.router.navigate(["/login"]);
        this.pageService.changeNotification("Successfully logged out");
        this.loggedIn = false;
      }
    );
  }
}
