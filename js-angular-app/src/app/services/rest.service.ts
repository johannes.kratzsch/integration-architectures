import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Employee } from "../models/employee";
import { Record } from "../models/record";
import { Order } from "../models/order";
import { PageService } from "./page.service";
import {Bonus} from "../models/bonus";
import {AuthorizationService} from "./authorization.service";

@Injectable({
  providedIn: 'root'
})
export class RestService {

  baseUrl = "http://localhost:3000"

  constructor(private http : HttpClient, private pageService : PageService, private authorizationService : AuthorizationService) { }

  createRecord() {

    return this.http.post(this.baseUrl + `/records`, this.pageService.currentRecord).subscribe(
      () => {
        this.pageService.changeNotification("Record created.");
        this.setRecordsListByEmployeeID(this.pageService.currentEmployee._id);
        this.setRecordByYearAndEmployeeID(this.pageService.currentRecord.year, this.pageService.currentEmployee._id);
      },
      () => {
        this.pageService.changeNotification("Record creation failed, read console for further information.");
      }
    );
  }

  updateRecord() {
    return this.http.put(this.baseUrl + `/records/${this.pageService.currentRecord._id}`, this.pageService.currentRecord).subscribe(
      () => {
        this.pageService.changeNotification("Record updated.");
      },
      () => {
        this.pageService.changeNotification("Record update failed, read console for further information.");
      }
    );
  }

  async setAllValuesForCurrentEmployeeAndYear() {

    if(!this.pageService.isCurrentEmployeeEmpty()) {
      this.setRecordByYearAndEmployeeID(this.pageService.currentRecord.year, this.pageService.currentEmployee._id).then();
      this.setOrders();
      this.setBonus();
      this.pageService.setSocialPerformanceBonus();
    }
  }

  updateEmployeeList() {
    return this.http.get(this.baseUrl + "/salesmen", {withCredentials: true}).subscribe(
      res => {

        let users = [];
        for(let obj of res as object[]) {
          let user:Employee = <Employee> obj;
          users.push(user);
        }

        this.pageService.setEmployeeList(users);
      }
    );
  }

  getRecords() {
    return this.http.get(this.baseUrl + "/records", {withCredentials: true}).subscribe(
      res => {
        let evaluationRecords = [];
        for(let obj of res as object[]) {
          let record:Record = <Record>obj;
          evaluationRecords.push(record);
        }

        this.pageService.setRecordList(evaluationRecords);
      }
    );
  }

  async fetchAndSetCurrentEmployee(id:number) {
    return this.http.get(this.baseUrl + `/salesmen/${id}`, {withCredentials: true}).toPromise().then(
      res => {
        let employee = <Employee> res;
        this.pageService.updateEmployeeDialog(employee);
      }
    );
  }

  setRecordByYearAndEmployeeID(year: Number, id: Number) {
    return this.http.get(this.baseUrl + "/records/employee/" + id + `/year/${year}`, {withCredentials: true}).toPromise().then(res => {
      let record: Record = <Record>res;

      this.pageService.currentRecord = record;
    }, () => {
      this.pageService.setCurrentRecordToDefault();
    });
  }

  setRecordsListByEmployeeID(id:Number) {
    return this.http.get(this.baseUrl + `/records/employee/${id}`, {withCredentials: true}).subscribe(
      res => {
        let evaluationRecords = [];
        for(let obj of res as object[]) {
          let record:Record = <Record>obj;
          evaluationRecords.push(record);
        }

        this.pageService.setRecordList(evaluationRecords);
        this.pageService.setSocialPerformanceBonus();
      }
    );
  }

  setOrders() {
    return this.http.get(this.baseUrl + `/orders/${this.pageService.currentRecord.salesman_id}/${this.pageService.currentRecord.year}`, {withCredentials: true}).subscribe(
      res => {
        this.pageService.orders = [];
        for(let obj of res as object[]) {
          let order:Order = <Order> obj;
          order.bonus = order.quantity * order.client_ranking * 0.05;
          this.pageService.orders.push(order);
        }
        this.pageService.setOrderBonus();
      }
    );
  }

  setBonus() {
    this.pageService.resetBonus();

    return this.http.get(this.baseUrl +
      `/bonus/${this.pageService.currentRecord.salesman_id}/${this.pageService.currentRecord.year}`, {withCredentials: true}).subscribe(
      res => {
        let savedBonus = <Bonus>res;
        console.log(`/bonus/${this.pageService.currentRecord.salesman_id}/${this.pageService.currentRecord.year}`)
        console.log(savedBonus)
        if (!savedBonus || ! savedBonus.approved) {
          return;
        } else {
          this.pageService.bonus = savedBonus;
        }
      }
    );
  }

  approveBonus(bonus: Bonus) {
    let body = {
      salesman_id : this.pageService.currentRecord.salesman_id,
      year: this.pageService.currentRecord.year,
      order_bonus : bonus.order_bonus,
      social_performance_bonus : bonus.social_performance_bonus,
      total_bonus : bonus.total_bonus,
      comment : bonus.comment,
      approved : true
    }

    return this.http.post(this.baseUrl + `/bonus`, body).subscribe(
      res => {
        let savedBonus = <Bonus>res;
        if (!savedBonus) { return; }

        if (savedBonus.approved) {
          this.pageService.bonus = savedBonus;
        } else {
          this.pageService.resetBonus();
        }
      }
    );
  }

}
