import { NgModule } from '@angular/core';

import { MatTableModule } from "@angular/material/table";
import { HttpClientModule } from "@angular/common/http";
import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from "./views/register/register.component";
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { UsertableComponent } from "./views/dashboard/usertable/usertable.component";
import { RecordtableComponent } from './views/dashboard/recordtable/recordtable.component';
import {FormsModule} from "@angular/forms";
import {BonusComponent} from "./views/dashboard/bonus/bonus.component";
import {OrdersComponent} from "./views/dashboard/orders/orders.component";
import { RecorddialogComponent } from './views/dashboard/recorddialog/recorddialog.component';
import { EmployeedialogComponent } from './views/dashboard/employeedialog/employeedialog.component';



@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        RegisterComponent,
        DashboardComponent,
        UsertableComponent,
        RecordtableComponent,
        BonusComponent,
        OrdersComponent,
        RecorddialogComponent,
        EmployeedialogComponent
    ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MatTableModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {

}
