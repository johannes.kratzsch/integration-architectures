import { Router } from "@angular/router";
import { AuthorizationService } from "../../services/authorization.service";
import { PageService } from "../../services/page.service";
import { HttpClient } from "@angular/common/http";
import {Component, OnDestroy} from "@angular/core";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  providers: [HttpClient],
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnDestroy {

  constructor(private route: Router,
              private pageService: PageService,
              private authorizationService: AuthorizationService)
  { }

  ngOnDestroy(): void {
    this.pageService.changeNotification("");
  }

  async login(email: string, password: string) {

    let loginMessage = await this.authorizationService.login(email, password);

    if (!loginMessage.successful) {
      this.pageService.changeNotification('Employee with this combination does not exist.');
      throw new Error("Could not login!");
    } else {
      if (!await this.authorizationService.isLoggedIn()) {
        this.pageService.changeNotification('Login failed, read the console.');
        throw new Error("Could not establish Session!");
      }
    }

    this.authorizationService.setActiveAccount(loginMessage.account);
    this.authorizationService.loggedIn = true;
    await this.route.navigate(['/dashboard']);
  }

}
