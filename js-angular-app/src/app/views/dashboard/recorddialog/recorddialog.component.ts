import { Component, OnInit } from '@angular/core';
import {PageService} from "../../../services/page.service";
import {AuthorizationService} from "../../../services/authorization.service";
import {RestService} from "../../../services/rest.service";

@Component({
  selector: 'app-recorddialog',
  templateUrl: './recorddialog.component.html',
  styleUrls: ['./recorddialog.component.css']
})
export class RecorddialogComponent implements OnInit {

  constructor(public pageService : PageService,
              public authorizationService : AuthorizationService,
              public restService : RestService) { }

  modifier : number = 20;

  ngOnInit(): void {
  }

  recalculateRecord() {
    if (this.pageService.isCurrentRecordEmpty()) {
      this.pageService.currentRecord.leadership_bonus = this.pageService.currentRecord.leadership * this.modifier;
      this.pageService.currentRecord.integrity_bonus = this.pageService.currentRecord.integrity * this.modifier;
      this.pageService.currentRecord.openness_bonus = this.pageService.currentRecord.openness * this.modifier;
      this.pageService.currentRecord.communications_bonus = this.pageService.currentRecord.communications * this.modifier;
      this.pageService.currentRecord.attitude_bonus = this.pageService.currentRecord.attitude * this.modifier;
      this.pageService.currentRecord.social_bonus = this.pageService.currentRecord.social * this.modifier;

      this.pageService.setSocialPerformanceBonus();
    }
  }

}
