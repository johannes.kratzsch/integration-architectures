import {Component, OnInit} from '@angular/core';
import { RestService } from "../../services/rest.service";
import {AuthorizationService} from "../../services/authorization.service";
import {Account} from "../../models/account";
import {PageService} from "../../services/page.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(public restService: RestService,
              public authorizationService : AuthorizationService,
              public pageService : PageService,
              private route: Router) {}

  async ngOnInit(): Promise<void> {
    if (!await this.authorizationService.isLoggedIn()) {
      this.pageService.changeNotification("Not logged in.");
      await this.route.navigate(["/login"]);
    }

    this.authorizationService.setActiveAccount(<Account>JSON.parse(<string>localStorage.getItem("activeAccount")));

    if (this.authorizationService.activeAccount.type == 'salesman') {
      await this.restService.fetchAndSetCurrentEmployee(this.authorizationService.activeAccount.employeeId);
      await this.restService.setRecordByYearAndEmployeeID(2021, this.authorizationService.activeAccount.employeeId);
    } else {
      this.restService.updateEmployeeList();
    }

    await this.restService.setAllValuesForCurrentEmployeeAndYear()
  }
}
