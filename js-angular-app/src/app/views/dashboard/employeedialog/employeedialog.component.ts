import { Component, OnInit } from '@angular/core';
import {PageService} from "../../../services/page.service";
import {RestService} from "../../../services/rest.service";

@Component({
  selector: 'app-employeedialog',
  templateUrl: './employeedialog.component.html',
  styleUrls: ['./employeedialog.component.css']
})
export class EmployeedialogComponent implements OnInit {

  years : number[] = []

  constructor(private restService : RestService, public pageService : PageService) {}

  onYearSelected() {
    this.restService.setAllValuesForCurrentEmployeeAndYear();
  }

  ngOnInit(): void {
    for (let i = new Date().getFullYear(); i >= 1900; i--) {
      this.years.push(i);
    }
  }

}
