import { Component, OnInit } from '@angular/core';
import {RestService} from "../../../services/rest.service";
import {PageService} from "../../../services/page.service";

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
  constructor(public restService : RestService, public pageService : PageService ) { }

  ngOnInit(): void {
  }

}
