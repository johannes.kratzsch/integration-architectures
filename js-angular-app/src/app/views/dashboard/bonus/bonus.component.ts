import { Component, OnInit } from '@angular/core';
import {RestService} from "../../../services/rest.service";
import {PageService} from "../../../services/page.service";
import {AuthorizationService} from "../../../services/authorization.service";

@Component({
  selector: 'app-bonus',
  templateUrl: './bonus.component.html',
  styleUrls: ['./bonus.component.css']
})
export class BonusComponent implements OnInit {
  constructor(public restService : RestService, public pageService : PageService, public authorizationService : AuthorizationService) { }

  ngOnInit(): void {
  }

  async approveBonus() {
    this.restService.approveBonus(this.pageService.bonus);
    this.restService.setBonus();
  }

}
