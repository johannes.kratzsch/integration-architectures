import { Component, OnInit } from '@angular/core';
import { Employee } from "../../../models/employee";
import { PageService } from "../../../services/page.service";
import {Record} from "../../../models/record";
import {RestService} from "../../../services/rest.service";

@Component({
  selector: 'app-usertable',
  templateUrl: './usertable.component.html',
  styleUrls: ['./usertable.component.css']
})
export class UsertableComponent implements OnInit {

  displayedColumns: string[] = ['_id', 'firstname', 'lastname', 'department'];

  constructor(public pageService : PageService, private restService : RestService) { }

  selectUser(row:Object) {
    let user:Employee = <Employee> row;
    this.restService.setRecordsListByEmployeeID(user._id);
    this.pageService.updateEmployeeDialog(user);

    this.pageService.updateRecordDialog(this.pageService.default_record, user._id)

    this.restService.setAllValuesForCurrentEmployeeAndYear();
  }

  ngOnInit(): void {
  }

}
