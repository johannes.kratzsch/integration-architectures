import { Component, OnInit } from '@angular/core';
import {PageService} from "../../../services/page.service";
import {Employee} from "../../../models/employee";
import {Record} from "../../../models/record";
import {RestService} from "../../../services/rest.service";

@Component({
  selector: 'app-recordstable',
  templateUrl: './recordtable.component.html',
  styleUrls: ['./recordtable.component.css']
})
export class RecordtableComponent implements OnInit {

  displayedColumns: string[] = ['_id', 'year'];

  constructor(public pageService : PageService, public restService : RestService ) { }

  selectRecord(row : Object) {
    let record:Record = <Record> row;
    //Prevent data binding
    this.pageService.updateRecordDialog(Object.assign({}, record), this.pageService.currentEmployee._id);
    this.restService.setAllValuesForCurrentEmployeeAndYear();
  }

  ngOnInit(): void {
  }

}
