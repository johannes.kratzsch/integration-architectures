import {Component, OnInit} from '@angular/core';
import {PageService} from "../../services/page.service";
import {AuthorizationService} from "../../services/authorization.service";
import {accountType} from "../../models/accountType";
import {Account} from "../../models/account";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {

  account_types = ['ceo', 'salesman', 'hr']

  constructor(private pageService : PageService, private restService : AuthorizationService) { }

  ngOnInit(): void {
  }

  register(email:string, password:string, firstname:string, lastname:string, type:string, employeeId:string) : void {

    let user:Account = {email:email, password:password, firstname:firstname, lastname:lastname, type: (<any>accountType)[type], employeeId: parseInt(employeeId)};

    this.restService.register(user);
    this.pageService.changeNotification('Successfully Registered!');
  }


}
