import {Account} from "../models/account";

export interface LoginMessage {

  successful : boolean;
  account : Account;
}
