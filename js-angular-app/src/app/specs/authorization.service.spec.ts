import { TestBed } from '@angular/core/testing';

import { AuthorizationService } from '../services/authorization.service';

describe('RestServiceService', () => {
  let service: AuthorizationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuthorizationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
