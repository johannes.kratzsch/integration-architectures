export interface Employee {

  firstname : string;
  lastname : string;
  department : string;

  _id : number;
}
