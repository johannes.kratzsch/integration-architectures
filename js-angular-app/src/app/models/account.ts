import {accountType} from "./accountType";

export interface Account {

   email : string;
   password : string;

   firstname : string;
   lastname : string;

   type : accountType;
   employeeId : number;

}
