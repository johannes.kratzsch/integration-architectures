export interface Order {
   product : string;
   quantity : number;
   client: string;
   client_ranking : number;
   bonus: number;
}
