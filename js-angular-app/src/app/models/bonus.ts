export interface Bonus {
  salesman_id : number;
  year: number;
  order_bonus : number;
  social_performance_bonus : number;
  total_bonus : number;
  comment : String;
  approved : boolean;
}
