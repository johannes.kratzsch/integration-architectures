export interface Record {
  _id: string;

  attitude : number;
  integrity : number;
  leadership : number;
  communications : number;
  openness : number;
  social : number;

  attitude_bonus : number;
  integrity_bonus : number;
  leadership_bonus : number;
  communications_bonus : number;
  openness_bonus : number;
  social_bonus : number;

  year: number;

  salesman_id : number;
}
