# Performance Monitor Anwendung starten
Folgende Schritte sind notwendig, um die Anwendung lokal auszuführen:
- Datenbank mit `mongod` starten
- Verbindung mit Fachbereichs-VPN herstellen
- Abhängigkeiten installieren:
`cd js-rest-service && npm install && cd ..` und `cd js-angular-app && npm install && cd ..`
- Angular App starten mit `cd js-angular-app && ng serve`
- REST API direkt in Intellij IDEA starten mit folgender Configuration:

![](docs/REST.png)