/*
* Quelle: https://stackabuse.com/building-a-rest-api-with-node-and-express/
*
*
* Wir werden nun eine einfache REST Schnittstelle mit Express.js bereitstellen.
* Dazu muss man zunächst folgendes in der Konsole eingeben:
* - npm init
* - npm install --save express
* - npm install --save body-parser
*
* Anschließend stellen wir mit folgendem Code einen Server zur Verfügung, der
* auf Anfragen lauscht:
* */


const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = 3000;

// Configuring body parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


app.get('/', (req, res) => {
	res.send('Antwort von Express');
});

app.post('/hello', (req, res) => {
	const person = req.body;
	console.log(person)

	// res.send(`Hello ${person.name}`);
	res.send(`Hello ${person.name}`);
});


app.listen(port, () => console.log(`Hello world app listening on port ${port}!`))


/*
* Diese API kann man nun mit Postman testen.
* Eine GET Anfrage auf localhost:3000 liefert nun unsere vorher festgelegte Antwort.
*
* Eine POST Anfrage auf localhost:3000/hello mit dem Body {"name":"Johannes"}
  liefert die Antwort "Hello Johannes".
* */