/*
* Quelle: https://www.digitalocean.com/community/tutorials/understanding-the-event-loop-callbacks-promises-and-async-await-in-javascript#promises
*
* Promises sind eine Alternative zu Callbacks. Sie haben den Vorteil, dass sie
* besser lesbar sind und zusätzliche Features mitbringen.
*
* Ein Promise ist ein Objekt, das zu einem späteren Zeitpunkt einen Wert zurückgeben kann
* (nämlich wenn der asynchrone Aufruf das Ergebnis geliefert hat).
*
*
* Nun werden wir ein Promise initialisieren,
* das direkt in den Status "Fulfilled" gebracht wird:
* */


const promise = new Promise((resolve, reject) => {
	resolve('Erfolg')
})


/*
* Ein Promise kann drei verschiedene Zustände haben: Pending, Fulfilled und Rejected.
*
* Wir wollen nun darauf reagieren, sobald das Promise fullfilled ist:
* */

promise.then((response) => {
	console.log(`${response}. Der API Call hat geklappt, jetzt können wir weitermachen.`)
})


/*
* Außerdem können die .then() Aufrufe auch verkettet werden (chained).
* Das nächste .then() geht dann erst los, wenn das vorherige verarbeitet wurde:
* */

promise
	.then((response) => {
		return response + ' und nochmal Erfolg'
	})
	.then((response) => {
		console.log(response)
	});


/*
* Außerdem können Fehler behandelt werden:
* */

const rejectingPromise = new Promise((resolve, reject) => {
	reject('Misserfolg')
})

rejectingPromise
	.then((respone) => {
		// Erfolg
	})
	.catch((error) => {
		console.log(error);
	});
