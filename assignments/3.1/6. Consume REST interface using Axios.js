/*
* Quelle: https://codesource.io/how-to-consume-restful-apis-with-axios/
*
* Zunächst müssen wir Axios installieren:
* - npm install --save axios
*
* Anschließend können wir den ersten API Call machen:
* */

const axios = require('axios');

let users = []; // names of users will be stored here
axios.get("https://jsonplaceholder.typicode.com/users")
	.then(({ data }) => {
		users = data.map(user => user.name); // get only the names of the users and store in an array
		console.log(users);
	})
	.catch(error=>{
		console.log(error);
	});



/*
* Nach Aktivierung des Fachbereichs-VPNs können wir auch auf OrangeHRM zugreifen:
* */

token = '17584c312fbaf848cda0df48cce92efbaeace330'
const config = {
	headers: { Authorization: `Bearer ${token}` }
};

axios.get(
	"https://sepp-hrm.inf.h-brs.de/symfony/web/index.php/api/v1/employee/3",
	config)
	.then((data) => {
		console.log(data.data);
	})
	.catch(error=>{
		console.log(error);
	});


/*
* Außerdem können wir die Daten eines Verkäufers ändern:
* */

const bodyParameters = {
	bonus: 500
};

axios.put(
	"https://sepp-hrm.inf.h-brs.de/symfony/web/index.php/api/v1/employee/3",
	config)
	.then((data) => {
		console.log(data.data);
	})
	.catch(error=>{
		console.log(error);
	});
