/*
* Quelle: https://www.digitalocean.com/community/tutorials/understanding-the-event-loop-callbacks-promises-and-async-await-in-javascript#async-functions-with-asyncawait
*
* Wenn man Promises nutzen möchte, aber nicht auf die gewohnte Syntax von JavaScript
* verzichten möchte, bietet sich await/async an.
*
* Eine mit async gekennzeichnete Funktion liefert als Ergebnis ein Promise:
*
* */

async function getInfo() {
	return {}
}
console.log(getInfo())




/*
* Das Schlüsselwort await ist das Äquivalent zu .then().
* Im nachfolgenden Beispiel wird damit
* auf die Erfüllung des Promise aus dem fetch()-Aufruf gewartet.
* */

global.fetch = require("node-fetch");

async function getSite() {
	const response = await fetch('https://google.com')
	const html = await response.text()

	console.log(html)
}

getSite()