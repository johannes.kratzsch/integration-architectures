function Person (name, age) {
	return {
		name: name,
		age: age
	}
}


// Option 1: Objekt direkt erzeugen:
var dieter = {
	name: "Dieter",
	age: 20
}

// Option 2: Objekt mithilfe des Person-Konstruktors erzeugen
var peter = new Person("Peter", 44);


// Option 3: Objekt anhand des Prototyps dieter erzeugen
var paula = Object.create(dieter);
paula.name = "Paula";
paula.age = 5;