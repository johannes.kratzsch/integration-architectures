/*
* Quelle: https://www.digitalocean.com/community/tutorials/understanding-the-event-loop-callbacks-promises-and-async-await-in-javascript#callback-functions
*
* JavaScript unterstützt kein Multithreading und daher gibt es nur einen einzigen
* Ausführungsfaden. Deswegen ist es immens wichtig, API Aufrufe asynchron abzuwickeln.
* Andernfalls müsste das gesamte Programm warten, bis die API Antwort angekommen ist.
* Ein Werkzeug zur Erzeugung von Asynchronität sind Callback handler.
*
* Nachfolgend kommt dazu ein Beispiel.
* Hier wird nach einer festgelegten Wartezeit der Callback handler aufgerufen.
* Die Funtion setTimeOut ist eine Higher Order Function, denn der erste Parameter ist eine
* Funktion (hier: ein Lambda Ausdruck)
* */


//
setTimeout(() => {
	console.log("Ich wurde aufgerufen!")
}, 1)


/*
* Das Problem bei dieser Schreibweise ist, dass sie sehr schnell unübersichtlich wird,
* sobald man mehrere Callbacks ineinander verschachtelt ("Pyramid of Doom"):
* */

setTimeout(() => {
	setTimeout(() => {
		setTimeout(() => {
			console.log("Ich wurde aufgerufen!");
		}, 1);
	}, 1);
}, 1);

